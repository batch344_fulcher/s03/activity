import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
    // Variables
        int integer = 0;
        Scanner in = new Scanner(System.in);

    // Try catch
        do {
            System.out.println("Input an integer whose factorial will be computed!");

            try {
                integer = in.nextInt();

            } catch (Exception e) {
                System.out.println("Invalid input");
                e.printStackTrace();
            }
        } while (integer < 1);

        // While loop
        int factorial = integer;
        int answer = 1;
        int counter = 1;

        while(counter <= integer){
            answer = answer * factorial;
            factorial--;
            counter++;
        }

        System.out.println("(While Loop) The factorial of " + integer + " is " + answer);

        // For Loop
        factorial = integer;
        answer = 1;

        for(counter = 1; counter <= integer; counter++){
            answer = answer * factorial;
            factorial--;
        }

        System.out.println("(For Loop) The factorial of " + integer + " is " + answer);
    }
}